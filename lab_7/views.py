from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'
    response = {"mahasiswa_list":mahasiswa_list, "friend_list":friend_list}
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

def mahasiswa_list_json(request, page):
	mahasiswa_list = csui_helper.instance.get_mahasiswa_list(page)
	data = {
		'mahasiswa_list': mahasiswa_list
	}
	return JsonResponse(data)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
